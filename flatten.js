/**
* @type {Express.Application}
*/
const { ExpressDataApplication } = require('@themost/express');
const container = require('../../server/app');
const { cloneDeep } = require('lodash');
const { MSSqlAdapter } = require('@themost/mssql');
const {
    SchemaLoaderStrategy,
    DataConfigurationStrategy,
    ODataModelBuilder,
    ODataConventionModelBuilder
} = require('@themost/data');
const { TraceUtils } = require('@themost/common');
const { writeFile } = require("fs/promises");
const path = require('path');
import config from './app.json'; 

function getFlatModel(context, name) {
    const model = context.model(name);
    // remove unused refs
    delete model._events;
    delete model._maxListeners;
    delete model._eventsCount;
    delete model.privileges;
    delete model.views;
    delete model.eventListeners;
    delete model.seed;
    delete model.sealed;
    delete model.abstract;

    // set attribute
    const fields = model.attributes.map((field) => cloneDeep(field)).map((field) => {
        if (field.primary === true && field.nullable !== false) {
            field.nullable = false;
        }

        if (field.mapping && model.inherits) {
            const parentModel = getParentModel(name, context)
            if (field.mapping.parentModel === parentModel.name) {
                field.mapping.parentModel = model.name;
            }

            if (field.mapping.childModel === parentModel.name) {
                field.mapping.childModel = model.name;
            }

            delete field.mapping.privileges;
        } else if (model.inherits) {
            const parentModel = getParentModel(name, context)
            // const parentModel = context.model(model.inherits);
            
            let mapping = parentModel.inferMapping(field.name);
            if (mapping) {
                if (mapping.parentModel === parentModel.name) {
                    mapping.parentModel = model.name;
                }

                if (mapping.childModel === parentModel.name) {
                    mapping.childModel = model.name;
                }

                delete mapping.privileges;
                Object.assign(field, {
                    mapping
                });
            } else {
                mapping = model.inferMapping(field.name);
                if (mapping && mapping.associationType === 'association') {
                    if (mapping.childModel !== model.name) {
                        mapping.childModel === model.name;
                    }

                    delete mapping.privileges;
                    Object.assign(field, {
                        mapping
                    });
                }
            }
        } else {
            const mapping = model.inferMapping(field.name);
            if (mapping) {
                delete mapping.privileges;
                Object.assign(field, {
                    mapping
                });
            }
        }

        delete field.model;
        return field;
    });
    // assign the fields
    Object.assign(model, {
        fields
    })
    delete model.inherits;
    delete model.implements;

    for (let field of model.fields) {
        const mapping = field.mapping;

        if (mapping && mapping.associationType === "association" && mapping.childField === field.name) {
            const referenceModel = context.model(mapping.parentModel);
            let referencedField = mapping.parentField;

            if (!referenceModel) {
                continue;
            }

            if (referencedField === "identifier") {
                referencedField = "id";
            }

            for (let parentField of referenceModel.fields) {
                if (parentField.name === referencedField) {
                    if (parentField.type !== field.type && parentField.type === "Counter") {
                        field.type = "Integer";
                    } else if (parentField.type !== field.type) {
                        field.type = parentField.type;
                    }
                    if (parentField.size) {
                        field.size = parentField.size;
                    }
                    break;
                }
            }
        }
    }

    model.source = model.name;
    model.viewName = model.viewAdapter;
    model.view = model.name;
    return model;
}

// find recursively the parent model in case of multilevel inheritance
function getParentModel(name, context) {
    const model = context.model(name);
    if (model.inherits == null) {
        return model;
    }
    return getParentModel(model.inherits, context);
}

(async function() {
    /**
    * @type {import("@themost/express").ExpressDataApplication}
    */
    const app = container.get(ExpressDataApplication.name);

    app.getConfiguration().useStrategy(ODataModelBuilder, ODataConventionModelBuilder);

    const schemaLoader = app
        .getConfiguration()
        .getStrategy(SchemaLoaderStrategy);

    /**
    * @type {DataContext}
    */
    const context = app.createContext();

    context.setDb(new MSSqlAdapter(
        config.adapters.filter(arr => arr.name === 'schema-relations').map(arr => arr.options)[0]
    ));
    const entityTypes = schemaLoader.getModels();
    const dataConfiguration = context.getConfiguration().getStrategy(DataConfigurationStrategy);

    for (let entityType of entityTypes) {
        let model = getFlatModel(context, entityType);
        dataConfiguration.setModelDefinition(model);

        model = context.model(entityType);
        await new Promise((resolve, reject) => {
            model.migrate(function(err) {
                if (err) {
                    return reject(err)
                }
                return resolve();
            })
        })
    }

    for (let entityType of entityTypes) {
        let model = context.model(entityType);

        for (let field of model.fields) {
            const mapping = field.mapping;
            if (mapping && mapping.associationType === "association" && mapping.childField === field.name) {
                const referenceModel = context.model(mapping.parentModel);
                if (!referenceModel) {
                    continue;
                }

                const table = model.name;
                let fieldName = mapping.childField;
                const referencedTable = referenceModel.name;
                let referencedField = mapping.parentField;

                if (referencedField === "identifier") {
                    referencedField = "id";
                }

                let FK = `FK_${table}.${fieldName}`;
                
                // append the FK (foreign key name) with the referenced table name if the don't match
                if (fieldName.toLowerCase() != referencedTable.toLowerCase()) {
                    FK += `_${referencedTable}`;
                }
                
                // drop the foreign key constraint if it already exists
                let sqlStatement =
                    `ALTER TABLE [${table}] ` +
                    `DROP CONSTRAINT IF EXISTS [${FK}];`;

                await context.db.executeAsync(sqlStatement);
                
                // add the foreign key constraint
                sqlStatement = 
                    `ALTER TABLE [${table}] ` +
                    `ADD CONSTRAINT [${FK}] FOREIGN KEY ([${fieldName}]) ` +
                    `REFERENCES [${referencedTable}](${referencedField});`;

                await context.db.executeAsync(sqlStatement);
            }
        }
    }
    // Write a script that creates the views that connect the tables created above with the respective views in the wanted database
    // so that it is possible to get data from them.
    const databaseToWrite = config.adapters.filter(arr => arr.name === 'data-views').map(arr => arr.options.write)[0];
    const databaseToRead = config.adapters.filter(arr => arr.name === 'data-views').map(arr => arr.options.read)[0];

    let sqlScript = `USE [${databaseToWrite}]\nGO\n\n`;
    for (let entityType of entityTypes) {
        const model = context.model(entityType);
        sqlScript += `IF EXISTS(SELECT * FROM sys.objects WHERE type='V' and name='${model.name}')\n` + 
            `DROP VIEW [${model.name}]\n` + 
            `GO\n\n` +
            `CREATE VIEW [dbo].[${model.name}] AS SELECT * FROM [${databaseToRead}].[dbo].[${model.viewName}]\n` +
            `GO\n\n`;
    }
    await writeFile(path.resolve(__dirname, 'createViewScript.sql'), sqlScript, (err) => { console.log(err) }); 
})().then(() => {
    TraceUtils.log('The operation was completed successfully.');
    process.exit(0);
}).catch((err) => {
    TraceUtils.error('The operation was completed with errors.');
    TraceUtils.error(err);
    process.exit(-1);
});
