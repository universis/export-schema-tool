
# Export Schema Tool
Export Schema Tool is a script used to create a "flattened" database schema, removing inheritance between tables and creating foreign keys constraints to give data models, the ability to be utilized by BI tools for analysis and graphical representation tools.

## Databases
The script needs two databases. The first one that we call **schema-relations** (both in the app.json config file and the script itself) is an existing empty database that the script will use to first create all the tables and then add the foreign key constraint relationships between their columns. We wanted the table names to be named after the model names, instead of having the "Data" and "Base" suffixes, so it's easier to understand the schema at first while reducing the tables name length by a bit which is convenient for diagrams and constraints. In result, the newly created tables have different names from the objects in the main database that hold the data and for that reason the script also generates an SQL script that is used to create views as selection of the main database objects. Those views will have the same name as the schema-relations tables, creating the need of a second database that we call as **data-views** (in the app.json and the script) as there can't be multiple objects with the same name in a database. That way, we can load the schema-relations database in a BI tool and if we want to do anything that requires data we can change the source of the table to the view with the same name in the data-views database.

## App.json
As described above the scripts uses two databases so in this section describes fields that need to be filled in the config file:
* **schema-relations**
  * server: The database server name or localhost if you are connecting locally
  * user: The user id of the account that is used to login to the server
  * password: The password of the account that is used to login to the server
  * database: The name of the database in the server that will be used for the schema
* **data-views**  
As mentioned in the **Database** section, the second database will contain views that are created as selection of the corresponding objects in the database that holds data, but are named after the **schema-relations** database tables. The **write** field in the config file is the name of the database that the new views will be created at and the **read** field is the name of the database that contains the objects with data and is used as a reference. 

## Foreign Keys
The foreign key names have 2 forms:
* FK_{Table}.{column}_{ReferencedTable}
* FK_{Table}.{column}

Since the column of the referenced Table is the primary key (which is the id) it's not mentioned in the foreign key name. If the name of the {column} matches the name of the {ReferencedTable} then the second form from above is chosen. 

E.g.: Lets say that the value of the tables and the columns are the following: 
* Table: Course
* Field: instructor
* ReferencedTable: Instructor
* ReferencedField: id

As mentioned above the referenced field is not being used as we feel that it is redundant since every foreign key name would have an **.id** suffix. Because the name of the column from the first table and the name of the referenced table match, the name of the foreign key will be **FK_Course.instructor** instead of **FK_Course.instructor_Instructor**.

## Installation
Clone this repository in the modules directory of the [universis-api](https://gitlab.com/universis/universis-api) project
```
git clone https://gitlab.com/universis/export-schema-tool.git
```

## Usage
In order to use the script for the Universis database you will need to:
* Have successfully installed the [universis-api](https://gitlab.com/universis/universis-api)
* Fill in the **schema-relations** fields in the app.json config with the server, database name and the user credentials for the schema database after acquiring the necessary rights for database access
* Fill in the read and write database names for **data-views** in the app.json, "write" being the one that we will contain the new views as described above and "read" being the main database used for reference
* Be able to ssh tunnel the Universis database server

To create the views after running the script, copy the content of the SQL script createViewScript.sql and run it in the preferred sql management tool. 

To run the script use the following command in the universis-api root directory:
```
node --require ./spec/helpers/babel --require ./spec/helpers/module-alias modules/export-schema-tool/flatten.js
```
or if you are in the export-schema-tool directory the following: 
```
node --require ./spec/helpers/babel --require ./spec/helpers/module-alias flatten.js
```